FROM microsoft/windowsservercore AS base

MAINTAINER Greg Farr <farr.gs@gmail.com>

# Install scoop
RUN @powershell -NoProfile -ExecutionPolicy unrestricted -Command "(iex (new-object net.webclient).downloadstring('https://get.scoop.sh')) >$null 2>&1" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin

# Install OpenJDK
#RUN scoop install openjdk
